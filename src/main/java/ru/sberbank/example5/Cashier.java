package ru.sberbank.example5;

import java.util.List;

public class Cashier {

    public double calculateSum(List<Object> purchases) {
        double resultSum = 0d;
        for (Object purchase : purchases) {
            if (purchase instanceof Product) {
                resultSum = sumProduct(resultSum, (Product) purchase);
            } else if (purchase instanceof Box) {
                resultSum = sumBox(resultSum, (Box) purchase);
            }
        }

        return resultSum;
    }

    private double sumProduct(double currentSum, Product product) {
        return currentSum + product.getPrice();
    }

    private double sumBox(double currentSum, Box box) {
        double resultSum = currentSum;
        for (Object purchase : box.getPurchases()) {
            if (purchase instanceof Product) {
                Product product = (Product) purchase;
                resultSum = sumProduct(resultSum, product);
            } else if (purchase instanceof Box) {
                resultSum = sumBox(resultSum, (Box) purchase);
            }
        }

        return resultSum;
    }
}
