package ru.sberbank.example1.chassis;

public class VanChassis {

    private String parts;

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
