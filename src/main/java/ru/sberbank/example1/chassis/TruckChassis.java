package ru.sberbank.example1.chassis;

public class TruckChassis {

    private String parts;

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
