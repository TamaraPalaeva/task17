package ru.sberbank.example1.chassis;

public class CarChassis {

    private String parts;

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
