package ru.sberbank.example1;

import ru.sberbank.example1.chassis.CarChassis;
import ru.sberbank.example1.chassis.TruckChassis;
import ru.sberbank.example1.chassis.VanChassis;
import ru.sberbank.example1.engine.CarEngine;
import ru.sberbank.example1.engine.TruckEngine;
import ru.sberbank.example1.engine.VanEngine;
import ru.sberbank.example1.vehicle.Car;
import ru.sberbank.example1.vehicle.Truck;
import ru.sberbank.example1.wheels.CarWheels;
import ru.sberbank.example1.wheels.TruckWheels;
import ru.sberbank.example1.wheels.VanWheels;
import ru.sberbank.example1.vehicle.Van;

public class Main {

    public static void main(String[] args) {
        CarChassis carChassis = new CarChassis();
        CarEngine carEngine = new CarEngine();
        CarWheels carWheels = new CarWheels();
        Car car = new Car(carChassis, carEngine, carWheels);
        System.out.println(car);

        VanChassis vanChassis = new VanChassis();
        VanEngine vanEngine = new VanEngine();
        VanWheels vanWheels = new VanWheels();
        Van van = new Van(vanChassis, vanEngine, vanWheels);
        System.out.println(van);

        TruckChassis truckChassis = new TruckChassis();
        TruckEngine truckEngine = new TruckEngine();
        TruckWheels truckWheels = new TruckWheels();
        Truck truck = new Truck(truckChassis, truckEngine, truckWheels);
        System.out.println(truck);
    }


}
