package ru.sberbank.example1.wheels;

public class CarWheels {

    private String parts;

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
