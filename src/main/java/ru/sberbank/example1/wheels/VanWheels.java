package ru.sberbank.example1.wheels;

public class VanWheels {

    private String parts;

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
