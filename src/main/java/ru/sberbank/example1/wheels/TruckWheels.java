package ru.sberbank.example1.wheels;

public class TruckWheels {

    private String parts;

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
