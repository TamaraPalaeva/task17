package ru.sberbank.example1.vehicle;

import ru.sberbank.example1.chassis.VanChassis;
import ru.sberbank.example1.engine.VanEngine;
import ru.sberbank.example1.wheels.VanWheels;

public class Van {

    private VanChassis chassis;
    private VanEngine engine;
    private VanWheels wheels;

    public Van(VanChassis chassis, VanEngine engine, VanWheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    public VanChassis getChassis() {
        return chassis;
    }

    public VanEngine getEngine() {
        return engine;
    }

    public VanWheels getWheels() {
        return wheels;
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + chassis +
                ", engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
