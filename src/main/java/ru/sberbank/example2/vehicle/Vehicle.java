package ru.sberbank.example2.vehicle;


import ru.sberbank.example2.brakes.ABS;
import ru.sberbank.example2.chassis.Chassis;
import ru.sberbank.example2.climate.AirConditioning;
import ru.sberbank.example2.climate.ClimateControlSystem;
import ru.sberbank.example2.climate.SeatHeating;
import ru.sberbank.example2.engine.Engine;
import ru.sberbank.example2.steering.Steering;
import ru.sberbank.example2.wheels.Wheels;

public abstract class Vehicle {

    private Chassis chassis;
    private Engine engine;
    private Wheels wheels;
    private Steering steering;
    private ClimateControlSystem climateControlSystem;
    private AirConditioning airConditioning;
    private SeatHeating seatHeating;
    private ABS abs;


    public Vehicle(Chassis chassis,
                   Engine engine,
                   Wheels wheels,
                   Steering steering,
                   ClimateControlSystem climateControlSystem,
                   AirConditioning airConditioning,
                   SeatHeating seatHeating,
                   ABS abs) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
        this.steering = steering;
        this.climateControlSystem = climateControlSystem;
        this.airConditioning = airConditioning;
        this.seatHeating = seatHeating;
        this.abs = abs;
    }

    public Chassis getChassis() {
        return chassis;
    }

    public Engine getEngine() {
        return engine;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public Steering getSteering() {
        return steering;
    }

    public ClimateControlSystem getClimateControlSystem() {
        return climateControlSystem;
    }

    public AirConditioning getAirConditioning() {
        return airConditioning;
    }

    public SeatHeating getSeatHeating() {
        return seatHeating;
    }

    public ABS getAbs() {
        return abs;
    }
}
