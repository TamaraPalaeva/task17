package ru.sberbank.example2.vehicle;

import ru.sberbank.example2.brakes.ABS;
import ru.sberbank.example2.chassis.Chassis;
import ru.sberbank.example2.climate.AirConditioning;
import ru.sberbank.example2.climate.ClimateControlSystem;
import ru.sberbank.example2.climate.SeatHeating;
import ru.sberbank.example2.engine.Engine;
import ru.sberbank.example2.steering.Steering;
import ru.sberbank.example2.wheels.Wheels;

public class Car extends Vehicle {


    public Car(Chassis chassis,
               Engine engine,
               Wheels wheels,
               Steering steering,
               ClimateControlSystem climateControlSystem,
               AirConditioning airConditioning,
               SeatHeating seatHeating,
               ABS abs) {
        super(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

    @Override
    public String toString() {
        return "Car{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                ", steering=" + getSteering() +
                ", climateControlSystem=" + getClimateControlSystem() +
                ", airConditioning=" + getAirConditioning() +
                ", seatHeating=" + getSeatHeating() +
                ", abs=" + getAbs() +
                '}';
    }

    public static class VanChassis {

        private String parts;

        public String getChassisParts() {
            return parts;
        }

        @Override
        public String toString() {
            return "VanChassis{" +
                    "parts='" + parts + '\'' +
                    '}';
        }
    }
}
