package ru.sberbank.example2.chassis;

public interface Chassis {

    String getChassisParts();
}
