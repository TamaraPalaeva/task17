package ru.sberbank.example2.climate;

public class ClimateControlSystem {

    private String parts;

    public ClimateControlSystem() {
        this.parts = "parts of climate control system";
    }

    public String getParts() {
        return parts;
    }
}
